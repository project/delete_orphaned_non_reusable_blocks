<?php

namespace Drupal\delete_orphaned_non_reusable_blocks\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\InlineBlockUsageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class DeleteOrphanedNonReusableBlocksForm.
 *
 * Form that displays the non reusable blocks that have been found.
 */
class DeleteOrphanedNonReusableBlocksForm extends FormBase {

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The Layout manager inline usage service definition.
   */
  protected InlineBlockUsageInterface $inlineBlockUsage;

  /**
   * Number of orphans that will be deleted.
   */
  protected int $orphanCount;

  /**
   * {@inheritdoc}
   */
  public function __construct(InlineBlockUsageInterface $inlineBlockUsage, EntityTypeManagerInterface $entityTypeManager) {
    $this->inlineBlockUsage = $inlineBlockUsage;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('inline_block.usage'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_orphaned_non_reusable_blocks_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $orphans = $this->getOrphans();

    $this->orphanCount = count($orphans);

    $form['warning1'] = [
      '#markup' => '<p>' . $this->t('Warning: This functionality is experimental and for use only when you know what you are doing.') . '</p>',
    ];

    $form['warning2'] = [
      '#markup' => '<p>' . $this->t('Do not under any circumstances use this on a production system. Take a database backup before use. You have been warned.') . '</p>',
    ];

    $header = [
      'block_id' => $this->t('Block ID'),
      'block_uuid' => $this->t('UUID'),
      'bundle' => $this->t('Bundle'),
    ];

    $form['orphan_table'] = [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $orphans,
      '#empty' => $this->t('No orphaned non reusable block content found'),
    ];

    if ($this->orphanCount) {

      $form['info'] = [
        '#markup' => '<p>' . $this->t('Confirming this action will delete %count orphaned non reusable block content entities from the system.', ['%count' => $this->orphanCount]) . '</p>',
      ];

      $form['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Delete orphans'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRedirect('delete_orphaned_non_reusable_blocks.confirm', [
      'orphanCount' => $this->orphanCount,
    ]);
  }

  /**
   * Get all orphaned reusable blocks.
   *
   * @return mixed[]
   *   An array containing details of any orphaned reusable blocks that
   *   were found.
   */
  private function getOrphans(): array {

    // Get all block content ids.
    $ids = $this->entityTypeManager->getStorage('block_content')->getQuery()->accessCheck(FALSE)->execute();

    $orphans = [];

    // Loop all fids and load files by fid.
    foreach ($ids as $id) {

      /** @var \Drupal\block_content\Entity\BlockContentInterface $block */
      $block = $this->entityTypeManager->getStorage('block_content')->load($id);

      // If this is a non-reusable block then it has been added in layout
      // builder.
      if (!$block->isReusable()) {

        // Get the usage for the block.
        $usage = $this->inlineBlockUsage->getUsage(intval($id));

        // Check if block not used and if not delete it.
        if (empty($usage) || (is_null($usage->layout_entity_id))) {

          $orphans[] = [
            'block_id' => $id,
            'block_uuid' => $block->uuid(),
            'bundle' => $block->bundle(),
          ];
        }
      }
    }

    return $orphans;
  }

}
