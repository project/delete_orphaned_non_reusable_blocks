<?php

namespace Drupal\delete_orphaned_non_reusable_blocks\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\layout_builder\InlineBlockUsageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Confirmation form to confirm deletion of orphaned non-reusable blocks.
 */
class DeleteOrphanedNonReusableBlocksConfirmForm extends ConfirmFormBase {

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Drupal\layout_builder\InlineBlockUsage definition.
   */
  protected InlineBlockUsageInterface $inlineBlockUsage;

  /**
   * Number of orphans that will be deleted.
   */
  protected int $orphanCount;

  /**
   * {@inheritdoc}
   */
  public function __construct(InlineBlockUsageInterface $inlineBlockUsage, EntityTypeManagerInterface $entityTypeManager) {
    $this->inlineBlockUsage = $inlineBlockUsage;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    // Instantiates this form class.
    return new static(
      // Load the service required to construct this class.
      $container->get('inline_block.usage'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_orphaned_non_reusable_blocks_confirm_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $orphanCount = NULL) {

    $this->orphanCount = $orphanCount;

    $form['info'] = [
      '#markup' => '<p>' . $this->t('Are you sure you want to delete %count orphaned non reusable block content items?', ['%count' => $this->orphanCount]) . '</p>',
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->deleteOrphans();

    $this->messenger()->addMessage('All orphaned non reusable block content entities have been deleted');

    $form_state->setRedirect('delete_orphaned_non_reusable_blocks');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('delete_orphaned_non_reusable_blocks');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Confirm deletion');
  }

  /**
   * {@inheritdoc}
   */
  private function deleteOrphans() {

    // Get all block content ids.
    $ids = $this->entityTypeManager->getStorage('block_content')->getQuery()->accessCheck(FALSE)->execute();

    // Loop all fids and load files by fid.
    foreach ($ids as $id) {

      /** @var \Drupal\block_content\Entity\BlockContentInterface $block */
      $block = $this->entityTypeManager->getStorage('block_content')->load($id);

      // If this is a non-reusable block then it has been added in layout
      // builder.
      if (!$block->isReusable()) {

        // Get the usage for the block.
        $usage = $this->inlineBlockUsage->getUsage(intval($id));

        // Check if block not used and if not delete it.
        if (empty($usage) || (is_null($usage->layout_entity_id))) {

          $this->logger('delete_orphaned_non_reusable_blocks')
            ->notice('Deleted orphaned non reusable block @blockid', [
              '@blockid' => $id,
            ]);

          $block->delete();
        }
      }
    }
  }

}
